<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
Use App\Models\Category;
use Illuminate\Support\Facades\View;
use DataTablables;

class CategoryController extends Controller
{
    protected $model;
    protected $view = 'categories.';
    protected $route = 'category.';

    public function __construct(Category $model)
    {
        $this->model = $model;

        View::share('view', $this->view);
        View::share('route', $this->route);
    }
    public function index()
    {
        $categories = $this->model->all();
        $no = 1;

        return view($this->view.'index', compact('categories', 'no'));
    }
    
    public function create()
    {
        return view($this->view.'create');
    }

    public function store(Request $request)
    {
        $category = $this->model->create($request->all());

        return redirect()->route($this->route.'index');
    }

    public function show($id)
    {
        
    }

    public function edit($id)
    {
        $category = $this->model->find($id);

        return view($this->view.'edit', compact('category'));
    }

    public function update(Request $request, $id)
    {
        $this->model->find($id)->update($request->all());

        return redirect()->route($this->route.'index');
    }

    public function destroy($id)
    {
        $this->model->find($id)->delete();

        return redirect()->route($this->route.'index');
    }
}
