<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

use App\Models\News;
use App\Models\Category;

class NewsController extends Controller
{
    protected $model;
    protected $view  = 'news.';
    protected $route = 'news.';

    public function __construct(News $model){
        $this->model = $model;

        View::share('route', $this->route);
        View::share('view', $this->view);
        View::share('category', Category::all());
        }

   
    public function index()
    {
        $news = $this->model->all();
        
        $no=1;

    	return view($this->view.'index', compact('news', 'no'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view($this->view.'create');
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        if($request->hasFile('image')){
            $resorce       = $request->file('image');
            $name   = $resorce->getClientOriginalName();
            $resorce->move(storage_path('app/public/news_image/'), $name);
            
            echo "Gambar berhasil di upload";
        }else{  
            echo "Gagal upload gambar";
        }
        
        $news = $this->model->create($data);
        
        return redirect()->route($this->route.'index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = $this->model->find($id);

        return view($this->view.'edit', compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        if($request->hasFile('image')){
            $resorce       = $request->file('image');
            $name   = $resorce->getClientOriginalName();
            $resorce->move(storage_path('app/public/news_image/'), $name);
            $data['image'] = $name;
            ;
        }else{  
            unset($data['image']);
        }
        
        $news = $this->model->find($id)->update($data);
        
        return redirect()->route($this->route.'index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = $this->model->find($id);
        $data->delete();
        return redirect()->back();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
}
