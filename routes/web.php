<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('front_end.index');
});

Route::resource('news', 'NewsController')->except('news');

Route::resource('category', 'CategoryController')->except('category');

Route::get('/detail/{id}', 'DashboardController@detail')->name('fornt_end.detail');
